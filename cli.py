#!/usr/bin/env python3
import issuu
import histograms
import matplotlib.pyplot as plt
from sys import argv
from getopt import getopt, GetoptError

TASKS = ["2a", "2b", "3a", "3b", "4d", "5", "6"]


def run_task(task: str) -> bool:
    fig, axs = plt.subplots()
    fig.canvas.set_window_title("Data Analytics")
    if not histograms.GeoGraph.set_data_path(file_name):
        return False
    histograms.GeoGraph.set_document_id(doc_id)
    if task == "2a":
        h = histograms.GeoGraph(axs)
        h.refresh()
        plt.show()
    elif task == "2b":
        h = histograms.RefinedGeoGraph(axs)
        h.refresh()
        plt.show()
    elif task == "3a":
        h = histograms.BrowserGraph(axs)
        h.refresh()
        plt.show()
    elif task == "3b":
        h = histograms.RefinedBrowserGraph(axs)
        h.refresh()
        plt.show()
    elif task == "4d":
        top = ["%d: %s" % (i + 1, v) for i, v in enumerate(issuu.issuu_also_like(file_name, doc_id, user_id))]
        if len(top):
            print(*top, sep='\n')
        else:
            print("No document found")
    elif task == "5":
        path = issuu.issuu_also_like_graph_gui(file_name, doc_id, user_id, False)
        print("Graph saved in:", path + ",", path.replace("gv", "ps"), "and", path + ".pdf")
    else:
        issuu.issuu_also_like_graph_gui(file_name, doc_id, user_id, True)


def usage():
    print(argv[0], "[-u user_uuid] -d doc_uuid -t task_id -f file_name")


if __name__ == "__main__":
    try:
        opts, args = getopt(argv[1:], 'u:d:t:f:')
    except GetoptError as err:
        print(err)
        usage()
        exit(2)
    user_id, doc_id, task_id, file_name = None, None, None, None
    for o, a in opts:
        if o == "-u":
            user_id = a
        elif o == "-d":
            doc_id = a
        elif o == "-t":
            if a not in TASKS:
                print("Task", a, "is not in the tasks list: [" + ", ".join(TASKS) + "]")
                exit(1)
            task_id = a
        elif o == "-f":
            file_name = a
    if not all((doc_id, task_id, file_name)):
        print("Missing parameter")
        usage()
        exit(2)
    try:
        res = run_task(task_id)
    except (RuntimeError, issuu.ExecutableNotFound):
        print("Error using the GraphViz tool, please verify installation.")
        exit(1)
    except (issuu.json.JSONDecodeError, KeyError):
        print("Error parsing file: Wrong format.")
        exit(1)
    except:
        print("Something unexpected happened.")
        exit(1)
    else:
        exit(0 if res else 1)
