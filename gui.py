#!/usr/bin/env python3
import tkinter as tk
import matplotlib

from gui import *

from histograms.graph import Graph


class Window(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.configure(background='white')
        self.wm_geometry("1280x720")
        self.title("ISSUU")

        self.data_path = ""

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.pages = {}

        for Page in (MenuPage, DocInfoPage, ReadInfoPage, SimDocPage, SimDocGraphPage):
            self.pages[Page] = Page(container, self)
            self.pages[Page].grid(row=0, column=0, sticky='nesw')

        self.valid_path = True
        self.show_page(MenuPage)
        self.valid_path = False

    def show_page(self, page):
        if self.valid_path:
            self.pages[page].tkraise()

    def back_to_menu(self):
        self.show_page(MenuPage)

    def update_path(self, path):
        self.data_path = path
        self.valid_path = Graph.set_data_path(path)


if __name__ == "__main__":
    matplotlib.use("TkAgg")

    window = Window()
    window.protocol('WM_DELETE_WINDOW', window.quit)
    window.mainloop()
