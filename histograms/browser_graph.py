from matplotlib.pyplot import FixedFormatter

from histograms.graph import Graph
import issuu


class BrowserGraph(Graph):

    _document_id = None

    def __init__(self, plot):
        super().__init__(plot)
        self.plot.set_title("Browsers")

    def format_data(self):
        return self._data

    @staticmethod
    def _refresh_data():
        if BrowserGraph._data_path is None:
            print("Cannot refresh: missing either data source or document id.")
        else:
            BrowserGraph._data = issuu.browser_raw_stats(Graph._data_path)

    def _format_x_axis(self, labels):
        self.plot.xaxis.set_major_formatter(FixedFormatter(
            list(map(lambda x: x if (len(x) < 13) else x[:10] + "...", labels))))

        for tick in self.plot.get_xticklabels():
            tick.set_rotation(90)
            tick.set_size(5)


class RefinedBrowserGraph(BrowserGraph):

    def __init__(self, plot):
        super().__init__(plot)
        self.plot.set_title("Refined Browsers")

    def format_data(self):
        return issuu.refine_raw_stats(self._data)
