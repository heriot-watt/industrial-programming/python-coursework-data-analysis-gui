from histograms.graph import Graph
import issuu


class GeoGraph(Graph):

    _document_id = None

    def __init__(self, plot):
        super().__init__(plot)
        self.plot.set_title("Countries")

    @staticmethod
    def set_document_id(document_id):
        GeoGraph._document_id = document_id
        GeoGraph._data = None

    def format_data(self):
        return self._data

    def refresh(self, document_id=None):
        if document_id is not None and document_id != GeoGraph._document_id:
            self.set_document_id(document_id)

        super().refresh()

    @staticmethod
    def _refresh_data():
        if GeoGraph._data_path is None or GeoGraph._document_id is None:
            print("Cannot refresh: missing either data source or document id.")
        else:
            GeoGraph._data = issuu.country_stats(Graph._data_path, GeoGraph._document_id)


class RefinedGeoGraph(GeoGraph):

    def __init__(self, plot):
        super().__init__(plot)
        self.plot.set_title("Continents")

    def format_data(self):
        return issuu.continents_stats_from_countries(self._data)
