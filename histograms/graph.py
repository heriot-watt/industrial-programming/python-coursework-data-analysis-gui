from matplotlib.pyplot import FixedLocator, FixedFormatter
from matplotlib.ticker import MaxNLocator
from abc import ABC, abstractmethod
from os import path
import numpy as np


class Graph(ABC):

    _data_path = None
    _data = None

    def __init__(self, plot):
        self.plot = plot

    @staticmethod
    def set_data_path(data_path) -> bool:
        if data_path.endswith('.json') and path.isfile(data_path):
            Graph._data_path = data_path
            Graph._data = None
            return True
        else:
            print("Wrong data source: file does not exist or is of the wrong format.")
            return False

    def empty_data(self):
        return True if self._data is None else len(self._data.keys()) == 0

    @abstractmethod
    def format_data(self):
        raise NotImplementedError("This is an abstract method, you need to implement it.")

    def refresh(self):
        if Graph._data is None:
            self._refresh_data()

        self._refresh_display(self.format_data())

    @staticmethod
    @abstractmethod
    def _refresh_data():
        raise NotImplementedError("This is an abstract method, you need to implement it.")

    def _format_x_axis(self, labels):
        self.plot.xaxis.set_major_formatter(FixedFormatter(labels))

    def _refresh_display(self, data):
        if data is not None and len(data.keys()) > 0:
            labels, values = zip(*sorted(data.items(), key=lambda x: x[1], reverse=True))
            x_indexes = np.arange(len(values))

            self.plot.bar(x_indexes, values, align="center")
            self.plot.yaxis.set_major_locator(MaxNLocator(integer=True))
            self.plot.xaxis.set_major_locator(FixedLocator(x_indexes))
            self._format_x_axis(labels)
        else:
            print("Nothing to display: no data.")
