#!/usr/bin/env python3
from issuu import *

if __name__ == "__main__":
    # print(issuu_also_like("sample_3m_lines.json", "140109173556-a4b921ab7619621709b098aa9de4d736"))
    issuu_also_like_graph_gui("sample_100k_lines.json", "100806162735-00000000115598650cb8b514246272b5", "00000000deadbeef", True)
    # print(issuu_all_docs("sample_600k_lines.json", "2765d917384e4a61"))
    # print(issuu_also_like("sample_100k_lines.json", "100806162735-00000000115598650cb8b514246272b5"))
    # print(issuu_also_like("sample_100k_lines.json", "100806162735-00000000115598650cb8b514246272b5", "00000000deadbeef"))
    # print(issuu_also_like("sample_600k_lines.json", "140207031738-eb742a5444c9b73df2d1ec9bff15dae9"))
