from collections import Counter, defaultdict
from itertools import chain
from math import log, floor
from graphviz import Digraph, ExecutableNotFound
from os import rename

import awoc
import orjson as json
import user_agents as ua

__countries = awoc.AWOC().get_countries()
__lines = 0


def issuu_reader(path: str) -> dict:
    """
    Generator yielding dictionaries from a given issuu JSON file
    :param path: The path of the file to read
    :return: List of issuu documents in a dictionary format
    """
    global __lines
    with open(path, 'rb') as file:
        __lines = 0
        for line in file:
            __lines += 1
            yield json.loads(line)


def issuu_matching_docs(path: str, doc_id: str) -> dict:
    """
    Generator yielding dictionaries that matched a given document id from a given issuu JSON file
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :return: List of issuu documents in a dictionary format matching the given id
    """
    yield from filter(lambda x: x["event_type"] == "read" and x["env_doc_id"] == doc_id, issuu_reader(path))


def country_stats(path: str, doc_id: str) -> Counter:
    """
    Counts how many times countries are represented for a given document in a given file
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :return: An object counting each different country
    """
    return Counter(map(lambda x: x["visitor_country"], issuu_matching_docs(path, doc_id)))


def __get_continent_from_iso2(name: str):
    """
    Gets the full name of a country from its ISO2 code
    :param name: The ISO2 code
    :return: The full name of the country
    """
    continent = next(filter(lambda x: x["ISO2"] == name, __countries), None)
    return continent["Continent Name"] if continent else "Unknown"


def continents_stats(path: str, doc_id: str) -> Counter:
    """
    Counts how many times continents are represented for a given document in a given file
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :return: An object counting each different continent
    """
    return Counter(
        map(__get_continent_from_iso2, map(lambda x: x["visitor_country"], issuu_matching_docs(path, doc_id))))


def continents_stats_from_countries(countries: Counter) -> Counter:
    """
    Counts how many times continents are represented from an object counting countries
    :param countries: Counter class containing a list of countries in ISO2 format
    :return: An object counting each different continent
    """
    return Counter(map(__get_continent_from_iso2, countries.elements()))


def browser_raw_stats(path: str) -> Counter:
    """
    Gets the list of browser agents from a list of issuu documents in a given file
    :param path: The path of the file to read
    :return: An object counting each different browser (user-agent)
    """
    return Counter(map(lambda x: x["visitor_useragent"] if x["visitor_useragent"] else "Unknown",
                       filter(lambda x: x["event_type"] == "read" and x["visitor_useragent"], issuu_reader(path))))


def refine_raw_stats(raw: Counter) -> Counter:
    """
    Refines a list of raw user agents into a list of Browser names
    :param raw: The object counting the user agents
    :return: An object counting each different browser
    """
    return Counter(map(lambda x: ua.parse(x).browser.family, raw.elements()))


def issuu_other_readers(path: str, doc_id: str) -> set:
    """
    Gives a list of users having read a specific document
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :return: A list of user ids
    """
    return set(map(lambda x: x["visitor_uuid"], issuu_matching_docs(path, doc_id)))


def issuu_all_docs(path: str, user_id: str) -> set:
    """
    Gives a list of documents read by a specific user
    :param path: The path of the file to read
    :param user_id: The id of the user to look for
    :return: A list of document ids
    """
    return set(map(lambda x: x["env_doc_id"], filter(
        lambda x: x["event_type"] == "read" and x["env_type"] == "reader" and x["visitor_uuid"] == user_id,
        issuu_reader(path))))


def __issuu_also_like_graph(path: str, doc_id: str, user_id: str = None) -> dict:
    """
    Gives a list of documents that were read by the same readers as the given document
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :param user_id: The user to exclude from the also like graph
    :return: A dictionary mapping each 'also likes' document to a list of readers
    """
    readers = issuu_other_readers(path, doc_id)
    m = defaultdict(set)
    if user_id and user_id in readers:
        readers.remove(user_id)
        m[doc_id].add(user_id)
    for doc in filter(lambda x: x["event_type"] == "read" and x["visitor_uuid"] in readers, issuu_reader(path)):
        m[doc["env_doc_id"]].add(doc["visitor_uuid"])
    return m


def __issuu_also_like_internal(doc: dict) -> list:
    """
    Make a top 10 of a given 'also likes' graph
    :param doc: The 'also likes' document
    :return: A list of the top 10 documents
    """
    return list(map(lambda x: x[0], sorted(Counter(chain.from_iterable(map(lambda t: [t[0]] * len(t[1]), doc.items())))
                    .most_common(),  key=lambda x: (x[1], x[0]), reverse=True)))[:10]


def issuu_also_like(path: str, doc_id: str, user_id: str = None) -> list:
    """
    Gives a list of the top 10 documents that were read by the same readers as the given document
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :param user_id: The user to exclude from the also like graph
    :return: A list of the top 10 documents
    """
    return __issuu_also_like_internal(__issuu_also_like_graph(path, doc_id, user_id))


def __human_format(number: int) -> str:
    """
    Format a given number into a specific human readable format
    :param number: The number to format
    :return: The formatted string
    """
    units = ['', 'k', 'm', 'g']
    k = 1000.0
    magnitude = int(floor(log(number, k)))
    return '%d%s' % (number / k ** magnitude, units[magnitude])


def issuu_also_like_graph_gui(path: str, doc_id: str, user_id: str = None, show: bool = False) -> str:
    """
    Generates a graphviz graph in the DOT format and generates a PostScript and PDF document with it
    :param path: The path of the file to read
    :param doc_id: The id of the object to look for
    :param user_id: The user to exclude from the also like graph
    :param show: Preview the generated graph or not
    :return: The path of the saved file
    """
    obj = __issuu_also_like_graph(path, doc_id, user_id)
    top = __issuu_also_like_internal(obj)
    for doc in list(obj.keys()):
        if doc not in top:
            del obj[doc]
    main = Digraph("also_likes")
    sub = Digraph()
    readers = Digraph()
    documents = Digraph()
    main.attr(ranksep=".75", ratio="compress", size="15,22", orientation="landscape", rotate="180")
    sub.attr('node', shape="plaintext", fontsize="16")
    sub.edge("Readers", "Documents", label="Size: " + __human_format(__lines))
    documents.attr(rank='same; Documents')
    for doc in top:
        if doc == doc_id:
            sub.node(doc[-4:], doc[-4:], style="filled", color=".3 .9 .7", shape="circle")
        else:
            sub.node(doc[-4:], doc[-4:], shape="circle")
        documents.node(doc[-4:])
    readers.attr(rank="same; Readers")
    for user in set(chain.from_iterable(obj.values())):
        if user == user_id:
            sub.node(user[-4:], user[-4:], style="filled", color=".3 .9 .7", shape="box")
        else:
            sub.node(user[-4:], user[-4:], shape="box")
        readers.node(user[-4:])
    sub.subgraph(readers)
    sub.subgraph(documents)
    edges = set()
    for k, v in obj.items():
        for user in v:
            edges.add((user[-4:], k[-4:]))
    for a, b in edges:
        sub.edge(a, b)
    main.subgraph(sub)
    main.render(format="png")
    main.render(format="ps2")
    rename("also_likes.gv.ps2", "also_likes.ps")
    main.render(cleanup=True, format="pdf", quiet_view=show, quiet=True)
    return main.save()

