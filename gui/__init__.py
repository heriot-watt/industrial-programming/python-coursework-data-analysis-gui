from gui.menu_page import MenuPage
from gui.doc_info_page import DocInfoPage
from gui.read_info_page import ReadInfoPage
from gui.sim_doc_page import SimDocPage
from gui.sim_doc_graph_page import SimDocGraphPage
