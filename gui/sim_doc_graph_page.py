import tkinter as tk
from PIL import ImageTk, Image
from tkinter import messagebox
from os import remove
from issuu import issuu_also_like_graph_gui, ExecutableNotFound
import issuu


class SimDocGraphPage(tk.Frame):

    def __init__(self, root, ctlr):
        tk.Frame.__init__(self, root)
        self.__ctlr = ctlr

        tk.Label(self, text="Similar Documents", font=("Helvetica", 24)).pack(pady=40, padx=10)
        tk.Button(self, text="Back to Menu", font=("Helvetica", 16), command=lambda: ctlr.back_to_menu()).pack()

        self.__user_id = tk.StringVar()
        self.__document_id = tk.StringVar()
        self.__document_id.set("100806162735-00000000115598650cb8b514246272b5")

        tk.Label(self, text="User ID", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__user_id).pack()

        tk.Label(self, text="Document ID", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__document_id).pack()

        tk.Button(self, text="Search", font=("Helvetica", 10), command=self.refresh).pack()

        tk.Label(self, text="Top 10 similar documents:", font=("Helvetica", 12)).pack(pady=20)

        self.__canvas = tk.Canvas(self, width=1260, height=400)
        self.__img = None
        self.__path = None

    def refresh(self):
        document_id = self.__document_id.get()
        user_id = self.__user_id.get()

        if self.__path:
            remove(self.__path)
        try:

            self.__path = issuu_also_like_graph_gui(self.__ctlr.data_path, doc_id=document_id,
                                                    user_id=user_id if user_id != "" else None) + ".png"
            self.__img = ImageTk.PhotoImage(Image.open(self.__path))
            self.__canvas.create_image(630, 200, anchor='center', image=self.__img)
            self.__canvas.pack(pady=10, padx=10)
        except (ExecutableNotFound, RuntimeError) as err:
            messagebox.showerror("Missing package", "Missing GraphViz package")
        except (issuu.json.JSONDecodeError, KeyError):
            messagebox.showerror("Wrong data format", "The data could not be correctly parsed")
