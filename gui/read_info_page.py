import tkinter as tk
from tkinter import messagebox

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

from histograms.browser_graph import BrowserGraph, RefinedBrowserGraph


class ReadInfoPage(tk.Frame):

    def __init__(self, root, ctlr):
        tk.Frame.__init__(self, root)
        self.__ctlr = ctlr

        tk.Label(self, text="Readers Info (Browsers)", font=("Helvetica", 24)).pack(pady=40, padx=10)
        tk.Button(self, text="Back to Menu", font=("Helvetica", 16), command=lambda: ctlr.back_to_menu()).pack()

        self.__value = tk.IntVar()
        tk.Radiobutton(self, text="Verbose Browsers", variable=self.__value, value=0, command=self.refresh).pack()
        tk.Radiobutton(self, text="Refined Browsers", variable=self.__value, value=1, command=self.refresh).pack()
        tk.Button(self, text="Refresh", font=("Helvetica", 10), command=self.refresh).pack()

        self.__plots = []

        fig1 = plt.figure(222)
        self.__plots.append({
            'path': ctlr.data_path,
            'figure': fig1,
            'graph': BrowserGraph(fig1.add_subplot()),
            'canvas': None
        })

        fig2 = plt.figure(333)
        self.__plots.append({
            'path': ctlr.data_path,
            'figure': fig2,
            'graph': RefinedBrowserGraph(fig2.add_subplot()),
            'canvas': None
        })

    def refresh(self):
        index = self.__value.get()
        for plot in self.__plots:
            if plot['canvas'] is not None:
                plot['canvas'].get_tk_widget().destroy()
                plot['canvas'] = None

        if self.__plots[index]['canvas'] is None or self.__plots[index]['path'] != self.__ctlr.data_path:
            self.__plots[index]['path'] = self.__ctlr.data_path
            self.__plots[index]['graph'].refresh()

            if not self.__plots[index]['graph'].empty_data():
                self.__plots[index]['canvas'] = FigureCanvasTkAgg(self.__plots[index]["figure"], self)

        if not self.__plots[index]['graph'].empty_data():
            self.__plots[index]['canvas'].get_tk_widget()\
                .pack(side='bottom', fill='both', padx=40, pady=40, expand=True)
        else:
            messagebox.showinfo("No data", "The document wasn't found in the reading list.")
