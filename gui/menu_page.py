import tkinter as tk


from gui.doc_info_page import DocInfoPage
from gui.read_info_page import ReadInfoPage
from gui.sim_doc_page import SimDocPage
from gui.sim_doc_graph_page import SimDocGraphPage


class MenuPage(tk.Frame):
    def __init__(self, root, ctlr):
        tk.Frame.__init__(self, root)

        tk.Label(self, text="Menu", font=("Helvetica", 24)).pack(pady=40, padx=10)

        self.__value = tk.StringVar()
        tk.Label(self, text="Data source path", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__value).pack()

        frame = tk.Frame(self)
        frame.place(anchor='c', relx=0.5, rely=0.5)

        tk.Button(frame, text="Documents Info (Countries and Continents)", font=("Helvetica", 16),
                  command=self.update_creator(ctlr, DocInfoPage))\
            .grid(row=2, column=0, pady=5, sticky='ew')

        tk.Button(frame, text="Readers Info (Browsers)", font=("Helvetica", 16),
                  command=self.update_creator(ctlr, ReadInfoPage))\
            .grid(row=3, column=0, pady=5, sticky='ew')

        tk.Button(frame, text="Similar Documents", font=("Helvetica", 16),
                  command=self.update_creator(ctlr, SimDocPage))\
            .grid(row=4, column=0, pady=5, sticky='ew')

        tk.Button(frame, text="Similar Documents Graph", font=("Helvetica", 16),
                  command=self.update_creator(ctlr, SimDocGraphPage))\
            .grid(row=5, column=0, pady=5, sticky='ew')

    def update_creator(self, ctlr, page):
        def update():
            ctlr.update_path(self.__value.get())
            ctlr.show_page(page)

        return update
