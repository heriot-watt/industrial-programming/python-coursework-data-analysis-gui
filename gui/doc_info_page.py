import tkinter as tk
from tkinter import messagebox

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

from histograms.geo_graph import GeoGraph, RefinedGeoGraph
import issuu


class DocInfoPage(tk.Frame):

    def __init__(self, root, ctlr):
        tk.Frame.__init__(self, root)

        tk.Label(self, text="Documents Info (Countries and Continents)", font=("Helvetica", 24)).pack(pady=40, padx=10)
        tk.Button(self, text="Back to Menu", font=("Helvetica", 16), command=lambda: ctlr.back_to_menu()).pack()

        self.__value = tk.StringVar()
        self.__value.set("140114165856-b27b7b1f16bc1b1286706ff33e2ced79")

        tk.Label(self, text="Document ID", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__value).pack()
        tk.Button(self, text="Search", font=("Helvetica", 10), command=self.refresh).pack()

        fig, axs = plt.subplots(ncols=2)
        self.__figure = fig
        self.__graphs = [GeoGraph(axs[0]), RefinedGeoGraph(axs[1])]
        self.__canvas = None

    def refresh(self):
        try:
            GeoGraph.set_document_id(self.__value.get())

            if self.__canvas is not None:
                self.__canvas.get_tk_widget().destroy()

            for graph in self.__graphs:
                graph.refresh()

            if not self.__graphs[0].empty_data():
                self.__canvas = FigureCanvasTkAgg(self.__figure, self)
                self.__canvas.get_tk_widget().pack(side='bottom', fill='both', padx=40, pady=40, expand=True)
            else:
                messagebox.showinfo("No data", "The document wasn't found in the reading list.")
        except (issuu.json.JSONDecodeError, KeyError):
            messagebox.showerror("Wrong data format", "The data could not be correctly parsed")