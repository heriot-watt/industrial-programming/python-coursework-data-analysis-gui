import tkinter as tk

from issuu import issuu_also_like


class SimDocPage(tk.Frame):

    def __init__(self, root, ctlr):
        tk.Frame.__init__(self, root)
        self.__ctlr = ctlr

        tk.Label(self, text="Similar Documents", font=("Helvetica", 24)).pack(pady=40, padx=10)
        tk.Button(self, text="Back to Menu", font=("Helvetica", 16), command=lambda: ctlr.back_to_menu()).pack()

        self.__user_id = tk.StringVar()
        self.__document_id = tk.StringVar()
        self.__document_id.set("100806162735-00000000115598650cb8b514246272b5")

        tk.Label(self, text="User ID", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__user_id).pack()

        tk.Label(self, text="Document ID", font=("Helvetica", 12)).pack(pady=5)
        tk.Entry(self, font=("Helvetica", 10), textvariable=self.__document_id).pack()

        tk.Button(self, text="Search", font=("Helvetica", 10), command=self.refresh).pack()

        tk.Label(self, text="Top 10 similar documents:", font=("Helvetica", 12)).pack(pady=20)
        self.__list = []

    def refresh(self):
        for elt in self.__list:
            elt.destroy()

        self.__list = []

        document_id = self.__document_id.get()
        user_id = self.__user_id.get()

        result = issuu_also_like(self.__ctlr.data_path, doc_id=document_id, user_id=user_id if user_id != "" else None)

        for i, elt in enumerate(result):
            self.__list.append(tk.Label(self, text="%d: %s" % (i + 1, elt), font=("Helvetica", 12)))

        if len(self.__list) == 0:
            self.__list.append(tk.Label(self, text="No data", font=("Helvetica", 12)))

        for elt in self.__list:
            elt.pack()
